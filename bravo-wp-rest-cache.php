<?php

/**
 * Plugin Name: BravoWP REST API Cache
 * License: GPL2+
 */

if(!defined('ABSPATH')) {
  exit;
}

if(!class_exists('BravoWP_REST_Cache') && function_exists('apc_store')) {

  class BravoWP_REST_Cache {

    public static function init() {
      add_filter('rest_pre_dispatch', array(__CLASS__, 'rest_pre_dispatch'), 10, 3);

      if(is_admin() && isset($_GET['bravo_wp_rest_cache_flush'])) {
        add_action('admin_notices', function() {
            ?>
            <div class="notice notice-success is-dismissible">
                <p><?php _e('Cache cleared!', 'BravoWP_REST_Cache'); ?></p>
            </div>
            <?php
        });
      }
    }

    public static function get_option($key, $default = '') {
      $result = function_exists('get_field') ? get_field($key, 'option') : null;
      return apply_filters($key, $default);
    }

    public static function get_includes() {
      return array_filter(array_map('trim', explode("\n", static::get_option('bravo_wp_rest_cache_include'))));
    }

    public static function get_excludes() {
      return array_filter(array_map('trim', explode("\n", static::get_option('bravo_wp_rest_cache_exclude'))));
    }

    public static function get_ttl() {
      return static::get_option('bravo_wp_rest_cache_ttl', 3600);
    }

    // Note sure about this.
    // The idea is that we return the cached result even if it has timed out, and then update the cache internally.
    public static function get_soft_expiry() {
      return static::get_option('bravo_wp_rest_soft_expiry', false);
    }

    public static function should_cache(WP_REST_Request $request) {
      $route = $request->get_route();
      $params = array_keys($request->get_params());
      if($request->get_method() !== 'GET') {
        return false;
      }
      if(in_array('disable-cache', $params)) {
        return false;
      }
      foreach(static::get_excludes() as $pattern) {
        if(strpos($route, $pattern) !== false) {
          return false;
        }
        if(in_array($pattern, $params)) {
          return false;
        }
      }
      foreach(static::get_includes() as $pattern) {
        if(strpos($route, $pattern) !== false) {
          return true;
        }
        if(in_array($pattern, $params)) {
          return true;
        }
      }
      return false;
    }

    public static function rest_pre_dispatch($result, WP_REST_Server $server, WP_REST_Request $request) {
      remove_filter('rest_pre_dispatch', array(__CLASS__, 'rest_pre_dispatch'));

      if(!static::should_cache($request)) {
        return $server->dispatch($request);
      }

      $cache_key = substr($request->get_route(), -20) . '_' . sha1(json_encode(array($request->get_route(), $request->get_params(), $request->get_headers())));

      if($entry = apc_fetch($cache_key)) {
        $ttl = static::get_ttl();
        $soft = static::get_soft_expiry();
        $valid = $ttl == 0 || time() - $entry['time'] < $ttl;

        if($valid || $soft) {
          if($soft) {
            // TODO: Add curl to WP-cron?
          }
          $content = json_decode($entry['content']);
          return new WP_REST_Response($content, $entry['status'], $entry['headers']);
        }
      }

      $result  = $server->dispatch($request);

      if($cache_key && $result->get_status() == 200) {
        apc_store($cache_key, array(
          'time' => time(),
          'status' => $result->get_status(),
          'headers' => headers_list(),
          'content' => json_encode($result->jsonSerialize()),
        ));
      }

      return $result;
    }

    public static function flush_cache() {
      apc_clear_cache();
    }
  }

  add_action('init', function() {
    BravoWP_REST_Cache::init();
  });

  add_action('save_post', function($post_id) {
    BravoWP_REST_Cache::flush_cache();
  });

  add_action('admin_bar_menu', function($admin_bar) {
    if(wp_get_current_user()->has_cap('edit_pages')) {
      $admin_bar->add_menu(array(
        'id' => 'cache-purge',
        'title' => 'Cache Purge',
        'href'=> add_query_arg('bravo_wp_rest_cache_flush', '1'),
      ));
    }
  }, 100);
}
